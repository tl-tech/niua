<center>
<img src="http://demo.niua.tech/niua/profile/upload/2023/02/20/1920c724-de44-4f71-b58f-fd614de52882.png">
<p/>
<h1>牛蛙后台管理系统</h1>
</center>

# niua简介及运行安装
> 牛蛙（niua）让编程从未如此简单

# niua项目简介

> niua项目是一套完全由java编写的后台管理系统，主要目的是提供一套完整的后台管理框架，
集成权限验证和代码生成，简化开发人员开发流程和工作量，让开发人员节省出更多时间，做更伟大的事。

[使用指南](https://www.niua.tech/)

https://www.niua.tech/

[演示地址](http://demo.niua.tech/)

http://demo.niua.tech/

账号和密码：admin/123456

# 项目图片
![](http://demo.niua.tech/niua/profile/upload/2023/02/20/3b1973fd-c6fc-4cab-97b9-fcdddb9a4230.png)
![](http://demo.niua.tech/niua/profile/upload/2023/02/20/84b929c2-181d-48f9-9d1b-26b75b6c6aaa.png)


# 主要模块说明
#### 1、模块说明
niua_admin 模块为业务模块，所有业务相关代码都写在里面

niua_auth  为权限验证模块，所有权限验证相关逻辑都写在里面

niua_common 为公用模块，可以公用的工具类和配置也在里面

niua_core 核心配置组件，项目核心的配置都写在里面

niua_gen 代码生成相关业务

niua_quartz 任务调度相关

easy-backend-ui 后台页面

docs 文档页面


#### 2、本地化部署需要注意的细节
admin模块下的yml文件

1）需要修改profile参数，此路径存储相关的上传和下载资源路径，需要根据自己的系统进行配置

2）gen.yml

修改outPath参数，此参数为代码生成的目标路径

#### 3、 初始化数据库
在niua-admin模块下的initsql文件夹下

#### 4、运行后台程序

在niua-admin模块下，运行NiuaApplication

#### 5、运行后台界面
* 先安装nodeJS，推荐版本为nodejs14版本.
* 进入easy-backend-ui目录
* 执行npm install
* 执行完毕后，在执行npm run serve
* 根据输出结果，访问前端页面

#### 6、docker部署

先进入docker-compose目录执行

1、构建 
```shell script
docker-compose build --no-cache
```


2、部署
```shell script
docker-compose up -d 
```

#### 6、xss相关配置

注解

可以使用 @XssCleanIgnore 注解对方法和类级别进行忽略。

针对某个 json 对象 String 字段处理
添加 @XssCleanIgnore 注解对路由忽略 xss 处理。
对需要处理得字段添加 @JsonDeserialize(using = XssCleanDeserializer.class) 注解。

参考文档：
https://gitee.com/596392912/mica/tree/master/mica-xss



变更
#2023-2-15

1、集成wangEditor

2、添加同一用户多终端同时登录功能

#2022-10-11

1、修复xss部分问题

2、修复登录提示部分问题

3、修复jdk1.8以上版本报错问题


  
  