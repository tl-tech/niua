const path = require('path')

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
    devServer: {
        proxy: process.env.VUE_APP_BASEURL
    },

    lintOnSave: false,

    configureWebpack: {
        name: 'icon',
        resolve: {
            alias: {
                '@': resolve('src'),
                'views': resolve('src/views')
            }
        }
    },

    chainWebpack(config) {

        config.module
            .rule('svg')
            .exclude.add(resolve('src/assets/icons'))
            .end()
        config.module
            .rule('icons')
            .test(/\.svg$/)
            .include.add(resolve('src/assets/icons'))
            .end()
            .use('svg-sprite-loader')
            .loader('svg-sprite-loader')
            .options({
                symbolId: 'icon-[name]'
            })
            .end()
    }
}
