import Vue from 'vue'

import Cookies from 'js-cookie'

import App from './App.vue'
import store from './store'
import router from './router'
import i18n from './lang'
//引入样式
import '@/styles/index.scss' // global css
//引入Element UI
import Element from 'element-ui'
import '@/styles/element-variables.scss'

import '@/assets/icons' //引入图标
//导入axios
import './plugins/axios.js'
//路由拦截
import './permission' // permission control
import qs from 'qs'
import Auth from './directives/auth'
import VueParticles from 'vue-particles'

Vue.use(VueParticles)
Vue.config.productionTip = false
Vue.prototype.$qs = qs
Vue.use(Auth);

//树型结构
import {handleTree,parseTime} from "@/utils/index";
Vue.prototype.handleTree = handleTree
Vue.prototype.parseTime = parseTime
//引入样式
import '@/styles/index.scss' // global css

//引入公共变量
import common from './plugins/common.js'
Vue.prototype.$common = common;

// 设置 element-ui 默认大小
// use添加i18n
Vue.use(Element, {
	size: Cookies.get('size') || 'small',
	i18n: (key, value) => i18n.t(key, value)
})

new Vue({
	store,
	router,
	i18n,
	render: h => h(App),
}).$mount('#app')
