import axios from "axios";
import {LOAD_RESOURCES_URL} from "@/utils/api";

//获取所有路由
export function getRoutes() {
  return axios({
    method: "GET",
    url: LOAD_RESOURCES_URL + "/0",
    data:{},
  });
}