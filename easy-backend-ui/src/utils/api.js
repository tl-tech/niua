// 登录注册相关接口
export const AUTH_LOGIN_URL =  "/auth/login";
export const AUTH_LOGOUT_URL =  "/auth/logout"; // 退出系统
export const FIND_USER_INFO =  "/auth/userInfo";//根据token获取用户信息


//加载权限资源
export const LIST_RESOURCES_URL =  "/resource/listResources";
export const LOAD_RESOURCES_URL =  "/resource/loadResources";
export const ADD_OR_EDITOR_RESOURCES_URL =  "/resource/saveOrUpdate";
export const DELETE_RESOURCES_URL =  "/resource/delete";
export const FIND_RESOURCES_URL =  "/resource/findResource";
export const BATCH_CREATE_URL =  "/resource/batchCreate";


//加载角色信息
export const LOAD_ALL_RESOURCES_URL =  "/role/loadAllResource";
export const LIST_ROLES_URL =  "/role/list";
export const LOAD_ROLES_RESOURCES_URL =  "/role/loadResources";
export const UPDATE_ROLES_RESOURCES_URL =  "/role/updateRoleResources"; // 更新权限绑定
export const FIND_ROLE_URL =  "/role/findRoleById"; // 根据角色id，查询角色信息
export const ADD_OR_EDITOR_ROLE_URL =  "/role/saveOrUpdate"; // 保存和更新角色信息
export const DELETE_ROLE_URL =  "/role/delete";
export const SELECT_ROLE_RESOURCES_URL =  "/role/selectResourceByRoleId"; // 查询角色拥有的全部权限信息，包括菜单和按钮的

//用户管理
export const LIST_USER_URL =  "/user/list";
export const FIND_USER_URL =  "/user/findUserById";
export const ADD_OR_EDITOR_USER_URL =  "/user/saveOrUpdate";
export const DELETE_USER_URL =  "/user/delete";
export const LOAD_ROLES_URL =  "/user/loadAllRoles";
export const UPDATE_USER_AND_ROLE =  "/user/updateUserAndRole";
export const MODIFY_PASSWORD =  "/user/modify-password";
export const RESET_PASSWORD =  "/user/resetPassword";


//代码自动生成
export const LIST_GENERATOR_URL =  "/generator/list";
export const DELETE_GENERATOR_URL =  "/generator/delete";
export const FIND_GENERATOR =  "/generator/findGeneratorById";
export const ADD_OR_EDITOR_GENERATOR_URL =  "/generator/saveOrUpdate";
export const AUTOTOOLS_GENERATOR_URL =  "/generator/autoGenerator";
export const BATCHDELETE_GENERATOR_URL =  "/generator/batchDelete";
export const QUERYTABLENAME_GENERATOR_URL =  "/generator/queryAllTableInDb";
export const EXPORT_GENERATOR_URL =  "/generator/export";

//上传图片
export const UPLOAD_URL =  "/niua/admin/common/upload";