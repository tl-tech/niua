import JSEncrypt from 'jsencrypt/bin/jsencrypt'
import CryptoJS from "crypto-js";

const CRYPTOJSKEY= "jKcJL*6OOX#sTv_@";

// 密钥对生成 http://web.chacuo.net/netrsakeypair

const publicKey = 'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALpiDCrZqu59meNHR3ys0ags/6be8cyn\n' +
  '9X4ySvhefrK1TjjLMIl3dgbsmRYAhg3y6lMTdGiA25yDxMqbbFXUubUCAwEAAQ=='

const privateKey = 'MIIBpjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQIguijvBWyUGcCAggA\n' +
    'MBQGCCqGSIb3DQMHBAjh+/2VMIKe3ASCAWDlvq5bHrdte9HJTe5Mb/CF3fEXURMt\n' +
    'Ahqzl6kg7A6CAU8PJNlYteZ7dzcrz+GE8eriWesgCyc0IFBRFwWAgeKz5nkxzpXQ\n' +
    'YFscKK6xc89DN4UcPAkAAYQybxuMpzZOz7KhnB69E2sEZ5ZKy5xXDPUPhEeMfASd\n' +
    'jCm9QBuMzgB2StPVbzlrK3512OhEY28NzvT7wNs+VgPpUmArGdy867F2+rN9xeLt\n' +
    'radQn+qUTLL6+eb3JMnP7uLj7bkCFzsNrqOM8jsSgTXROdOlIs1AE9RGyuzhWFKk\n' +
    'T7wtJJOL1MVtess+AtMBv38RCAudNTYEa4EndpA3HdZagKhpYM0PvkGQgGabrsRK\n' +
    'U6+q87XlUJHhvU0vcj1PhaTiw7xFMmKAFb8wNUQAPHrqlF2+HHK8o3msJII+QG6f\n' +
    '0xHAdaBijSStEOWnbLECnlQrhs93GEzHNuN8f3ZyOBfmlhyJ0PtXIMv1'

// 加密
export function encrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPublicKey(publicKey) // 设置公钥
  return encryptor.encrypt(txt) // 对需要加密的数据进行加密
}

// 解密
export function decrypt(txt) {
  const encryptor = new JSEncrypt()
  encryptor.setPrivateKey(privateKey)
  return encryptor.decrypt(txt)
}


export function aesEncrypt(plaintText) {
  var options = {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
  };
  var key = CryptoJS.enc.Utf8.parse(CRYPTOJSKEY);
  var encryptedData = CryptoJS.AES.encrypt(plaintText, key, options);
  var encryptedBase64Str = encryptedData.toString().replace(/\//g, "_");
  encryptedBase64Str = encryptedBase64Str.replace(/\+/g,"-");
  return encryptedBase64Str;
}
//解密
export function aesDecrypt(encryptedBase64Str) {
  var vals = encryptedBase64Str.replace(/-/g, '+').replace(/_/g, '/');
  var options = {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
  };
  var key = CryptoJS.enc.Utf8.parse(CRYPTOJSKEY);
  var decryptedData = CryptoJS.AES.decrypt(vals, key, options);
  var decryptedStr = CryptoJS.enc.Utf8.stringify(decryptedData);
  return decryptedStr
}
