export default {
    login: {
        title: 'NIUA后台管理系统',
        logIn: '登录',
        username: '请输入用户名',
        password: '请输入密码',
        need: '请填写用户名和密码',
        trueActPwd: '请输入正确的用户名和密码',
        verifyCode: '请输入验证码',
        falseVC: '验证码错误',
        vcNull: '验证码不能为空',
        userDisabled: '当前用户不可用',
        tip: '提示',
        success: '登录成功'
    },
    navbar: {
        account: '账号',
        changePassword: '修改密码',
        logOut: '退出登录',
        resetPassword: '重置密码',
        oldPassword: '旧密码',
        oldPlace: '请输入旧密码',
        newPassword: '新密码',
        newPlace: '请输入新密码',
        repeatPassword: '确认密码',
        repeatPlace: '请再次输入新密码',
        confirm: '确认修改',
        reset: '重置',
        confirmAgain: '请确认新密码',
        resetSuccess: '重置密码成功',
        passwordLen: '用户密码的长度在6～18个字符',
        diffPassword: '两次输入密码不一致!'
    }
}