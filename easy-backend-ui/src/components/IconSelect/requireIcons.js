const req = require.context('../../assets/icons/svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys()

const re = /\.\/(.*)\.svg/

let icons = requireAll(req).map(i => {
    return i.match(re)[1]
})
import iconList from "./icon"
iconList.forEach((item)=> {
    icons.push(item.name)
})

export default icons