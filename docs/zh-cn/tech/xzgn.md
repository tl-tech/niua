# 功能组件

## 添加功能

对于添加功能，我们将添加与修改共用后端接口，在前端部分做区分处理。

函数主体

```javascript
//添加事件
addHandleClick: function () {
      this.dialogForm = this.$options.data().dialogForm;
      this.$data.isEditor = false;
      this.$data.dialogAddFormVisible = true;
      this.resetDialogFormData('dialogForm');
    },
//添加函数
saveOrUpdate: function () {
      this.$refs['dialogForm'].validate((valid) => {
        if (valid && this.checkDialogForm()) {
          this.axios({
            method: 'POST',
            url: this.$data.urls.saveOrUpdate,
            data: this.dialogForm
          }).then(res => {
            let code = res.data.code
            if (code == 200) {
              this.pageList();
              this.$data.dialogAddFormVisible = false
            } else if (code == 20004) {
              this.$message.error('请先修改数据在更新');
            }
          }).catch(error => {
            console.log(error);
          });
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
```

后端代码：

```java
    /**
    * 添加修改
    * @param test
    * @return
    */
    @Log(value = "添加修改", businessType = BusinessType.INSERTORUPDATE)
    @PreAuthorize("hasAuthority('/test/saveOrUpdate')")
    @NoRepeatSubmit
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @PostMapping("/saveOrUpdate")
    public ResultJson saveOrUpdate(@RequestBody Test test){
        boolean flag = testService.saveOrUpdate(test);
        if(flag){
            return ResultJson.ok();
        }
        return ResultJson.failure(ResultCode.NOT_UPDATE);
    }
```

参数说明：

-  `dialogForm`需要传给后台的json数据，里面定义封装好的对象的属性
- `test`后端以test实体接收前端传输的json数据

总结：`addHandleClick`方法用于弹出添加类型的添加框，里面进行了`dialogForm`重置，是否为修改框的判断字段`isEditor`设置为false，用于显示添加修改框的字段`dialogAddFormVisible`设置为true。

调用方式：

```vue
//通过点击事件调用addHandleClick
<el-button v-auth="['/test/saveOrUpdate']" type="primary" @click="addHandleClick">添加</el-button>
//弹出框的确定按钮调用saveOrUpdate
<el-button type="primary" @click="saveOrUpdate">确 定</el-button>
```




