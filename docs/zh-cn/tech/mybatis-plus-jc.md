# 集成 mybatis-plus 实现对 mybatis 的增强

> `Mybatis-Plus` 是在 `Mybatis` 的基础上进行扩展，并且可以兼容原生 `Mybatis`
>  
> 同时支持通用的 `CRUD` 操作、多种主键策略、分页等功能。
>  
> 极大帮助我们简化了开发流程

1. `pom.xml` 添加依赖坐标

```java
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.3.1.tmp</version>
</dependency>
```

2. `niua-admin` 模块中 `application.yml` 文件

```yaml
# mybatis-plus配置
mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
  # 搜索指定包别名
  typeAliasesPackage: tech.niua.auth.domain
  # 配置mapper的扫描，找到所有的mapper.xml映射文件
  mapperLocations: classpath*:mybatis/**/*Mapper.xml
  # 加载全局的配置文件
#  configLocation: classpath:mybatis/mybatis-config.xml
```

3. 配置自动填充创建时间与更新时间字段

```java
package tech.niua.core.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;
import java.time.LocalDateTime;
@Configuration
public class MybatisObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        setFieldValByName("createTime", LocalDateTime.now(),metaObject);
        setFieldValByName("updateTime",LocalDateTime.now(),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValByName("updateTime", LocalDateTime.now(),metaObject);
    }

}
```

4. `Mybatis-Plus` 分页插件配置

```java
package tech.niua.auth.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@Configuration
@MapperScan(value = {"tech.niua.**.mapper"})
public class MybatisPlusConfig {

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        // 设置请求的页面大于最大页后操作， true调回到首页，false 继续请求  默认false
        // paginationInterceptor.setOverflow(false);
        // 设置最大单页限制数量，默认 500 条，-1 不受限制
        // paginationInterceptor.setLimit(500);
        // 开启 count 的 join 优化,只针对部分 left join
        paginationInterceptor.setCountSqlParser(new JsqlParserCountOptimize(true));
        return paginationInterceptor;
    }
}

```

## 示例 Controller (使用mybatis-plus进行CRUD)

```java
package tech.niua.admin.test.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import tech.niua.admin.test.domain.Test;
import tech.niua.admin.test.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import tech.niua.core.enums.BusinessType;
import tech.niua.common.model.ResultCode;
import tech.niua.common.model.ResultJson;
import java.util.Arrays;
import tech.niua.common.utils.poi.ExcelUtil;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import tech.niua.core.annotation.NoRepeatSubmit;
import tech.niua.core.annotation.Log;



/**
 * <p>
 *  测试表 控制类
 * </p>
 *
 * @author niua
 * @since 2022-07-19 16:50:39
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private ITestService testService;

    /**
    * 查询列表
    *
    * @param currentPage
    * @param pageSize
    * @param test
    * @return
    */
    @Log(value = "查询列表", businessType = BusinessType.LIST)
    @PreAuthorize("hasAuthority('/test')")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @PostMapping("/list/{currentPage}/{pageSize}")
    public ResultJson index(@PathVariable Integer currentPage, @PathVariable Integer pageSize,@RequestBody  Test test) {
        QueryWrapper<Test> queryWrapper = new QueryWrapper<>();
                if (StringUtils.isNotBlank(test.getName())) {
                queryWrapper.like("name", test.getName());
                }
                if (test.getCreateTimeBegin() != null && test.getCreateTimeEnd()  != null ){
                queryWrapper.between("create_time", test.getCreateTimeBegin(), test.getCreateTimeEnd());
                }
                queryWrapper.orderByDesc("create_time");
                queryWrapper.eq("delete_flag",0);
        IPage<Test> pageList = testService.page(new Page<>(currentPage, pageSize), queryWrapper);
        return ResultJson.ok(pageList);
    }

    /**
    *根据id查找
    * @param: id
    * @return
    */
    @PreAuthorize("hasAuthority('/test')")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @GetMapping("/findById/{id}")
    public ResultJson findTestById(@PathVariable Long id) {
        Test test = testService.getById(id);
        if(test != null){
            return ResultJson.ok(test);
        }
        return ResultJson.failure(ResultCode.BAD_REQUEST);
    }

    /**
    * 添加修改
    * @param test
    * @return
    */
    @Log(value = "添加修改", businessType = BusinessType.INSERTORUPDATE)
    @PreAuthorize("hasAuthority('/test/saveOrUpdate')")
    @NoRepeatSubmit
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @PostMapping("/saveOrUpdate")
    public ResultJson saveOrUpdate(@RequestBody Test test){
        boolean flag = testService.saveOrUpdate(test);
        if(flag){
            return ResultJson.ok();
        }
        return ResultJson.failure(ResultCode.NOT_UPDATE);
    }

    /**
    * 删除
    * @param ids
    * @return
    */
    @Log(value = "删除", businessType = BusinessType.DELETE)
    @PreAuthorize("hasAuthority('/test/delete')")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @GetMapping("/delete")
    public ResultJson delete(@RequestParam("ids") Long[] ids){
        UpdateWrapper<Test> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id",ids).set("delete_flag",1);
        boolean update = testService.update(null, updateWrapper);
        if(update) return ResultJson.ok();
        return ResultJson.failure(ResultCode.SERVER_ERROR);
    }

    /**
    * 数据导出
    * @return
    */
    @Log(value = "数据导出", businessType = BusinessType.EXPORT)
    @PreAuthorize("hasAuthority('/test/export')")
    @GetMapping("/export")
    public ResultJson export(Test test) {
        List<Test> list = testService.list();
        ExcelUtil<Test> util = new ExcelUtil<>(Test.class);
        return util.exportExcel(list, "自动生成测试表数据");
    }

}


```