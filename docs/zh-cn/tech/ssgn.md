# 搜索功能

对于大量的数据列表而言，通常我们需要快速精准地筛选我们需要的数据而非一逐一查找，因此在框架中集成了搜索功能。

函数主体：

```javascript
//列表显示
pageList: function () {
      this.$data.loading = true;
      let url = this.$data.urls.search + "/" + this.currentPage + "/" + this.pageSize;
      this.axios({
        method: "POST",
        url: url,
        data: this.$data.searchForm,
      }).then((res) => {
        this.$data.loading = false;
        let code = res.data.code;
        if (code == 200) {
          this.$data.tableData = res.data.data.records;
          this.$data.totalCount = res.data.data.total;
          this.$data.currentPage = res.data.data.current;
        }
      }).catch((error) => {
        console.log(error);
      });
    },
    //查询
    onSearch: function () {
      this.pageList();
    },
```

后端代码

```
    /**
    * 查询列表
    *
    * @param currentPage
    * @param pageSize
    * @param test
    * @return
    */
    @Log(value = "查询列表", businessType = BusinessType.LIST)
    @PreAuthorize("hasAuthority('/test')")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @PostMapping("/list/{currentPage}/{pageSize}")
    public ResultJson index(@PathVariable Integer currentPage, @PathVariable Integer pageSize,@RequestBody  Test test) {
        QueryWrapper<Test> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(test.getName())) {
            queryWrapper.like("t_test.name", test.getName());
        }
        if (test.getAge() !=null) {
            queryWrapper.like("t_test.age", test.getAge());
        }
        if (StringUtils.isNotBlank(test.getSex())) {
            queryWrapper.like("t_test.sex", test.getSex());
        }
        if (test.getCreateTimeBegin() != null && test.getCreateTimeEnd()  != null ){
            queryWrapper.between("t_test.create_time", test.getCreateTimeBegin(), test.getCreateTimeEnd());
        }
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("t_test.delete_flag",0);
        IPage<Test> pageList = testService.page(new Page<>(currentPage, pageSize), queryWrapper);
        return ResultJson.ok(pageList);
    }
```

参数说明：

- `searchform`查询对象，内有需要查询的属性

总结：在vue部分将输入框、搜索框等内容绑定到`searchform`对应的属性中，`pagelist`函数将`searchform`作为参数整个传给后端，后端会根据`searchform`内与`test`实体对应上的属性作为查询条件注入到`queryWrapper`中，最后将`queryWrapper`作为参数通过分页方法获取符合条件的数据，再返回给前端，`pagelist`接收返回数据后将对象集合存储到`tableData`数组中，之后通过vue显示数据。

调用：

```vue
    <el-row>
      <el-form ref="searchForm" :inline="true" :model="searchForm" class="demo-form-inline">
        <el-form-item label="名字" prop="name">
        //将input的内容通过v-model绑定到searchForm的属性中
          <el-input v-model.trim="searchForm.name"
                    placeholder="名字">
          </el-input>
        </el-form-item>
        <el-form-item label="年龄" prop="age">
          <el-input v-model.trim="searchForm.age"
                    placeholder="年龄">
          </el-input>
        </el-form-item>
        <el-form-item label="性别" prop="sex">
          <el-input v-model.trim="searchForm.sex"
                    placeholder="性别">
          </el-input>
        </el-form-item>
        <el-form-item label="创建时间" prop="createTime">
          <el-date-picker
              v-model.trim="searchDate"
              end-placeholder="结束日期"
              range-separator="至"
              start-placeholder="开始日期"
              type="datetimerange"
              unlink-panels
              value-format="yyyy-MM-dd HH:mm:ss"
              @change="dateChange()">
          </el-date-picker>
        </el-form-item>
        <el-form-item>
        //绑定onSearch函数到查询按钮的点击事件
          <el-button type="primary" @click="onSearch">查 询</el-button>
        </el-form-item>
      </el-form>
    </el-row>
```
