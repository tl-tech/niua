# 环境部署

---

## 准备工作

- JDK >= 1.8 (推荐1.8版本)
- MySql >= 5.7.0 (推荐5.7和8.0版本)
- Redis >= 3.0
- Maven >= 3.0
- Node >= 12
- Python >= 2.7

> **Tips**
> 
> 前端安装完 `node.js` 后，国内用户建议设置好淘宝源，不建议直接使用 `cnpm` (高科技访问请忽略)


## 运行系统

---

前往 [Gitee](https://gitee.com/wilson2016/niua)进行克隆项目或下载项目

### 后端运行

1. 使用 `IDEA` 打开克隆的项目
2. 使用 `Navicat` 创建数据库 `niua-easy` 
> `character set` 设置为 `utf8mb4`
> 
> `collation` 设置为 `utf8mb4_general_ci`

3. 导入 `sql` 文件
> `sql` 文件存放路径在项目的 `niua-admin/src/main/resources/initsql/niua-easy.sql`

4. 进入 `niua-admin/src/main/java/tech/niua/` 找到启动类 `NiuaApplication` 启动类进行启动
5. 直到控制台循环输出 *牛蛙框架，正在执行中.......* 即为启动成功

### 前端运行

```bash
# 进入前端项目目录
cd easy-backend-ui

# 安装依赖
npm install

# 启动前端项目
npm run serve
```

> 过高版本可能会导致 `npm install` 失败，*Error: Node Sass version 7.0.1 is incompatible with ^4.0.0异常*
> 
> 解决方案如下：
>> ```bash
>> # 卸载node-sass
>> npm uninstall node-sass
>> 
>> # 安装sass
>> npm install sass@1.26.5 --save-dev
>> 
>> # 安装sass-loader
>> npm install sass-loader@7.3.1
>> ```
>> 执行 `npm install` 和 `npm run serve` 测试是否正常启动

打开浏览器，输入 [http://localhost:8080](http://localhost:8080)进入登陆页面

> 默认账号：`admin`
> 
> 默认密码：`123456`

若能正常打开登陆页面，且可以登陆成功，菜单页面展示正常，则代表环境搭建成功

> **Tips**
> 
> 因为该项目是前后端分离的，所以需要前端和后端分别启动成功之后，才可以进行访问

### 必要配置

- 修改所要使用的 `application.yml` 文件

在 `niua-admin/src/main/resources/` 目录下，找到 `application.yml` 文件

```yml
spring:  
  profiles:  
    active: local
```

修改其中 `active` 项，若为 `local` 则代表要使用的是 `application-local.yml` 文件

- 修改 `application-local.yml` 文件

```yml
primary: master #设置默认的数据源或者数据源组,默认值即为master  
strict: false #严格匹配数据源,默认false. true未匹配到指定数据源时抛异常,false使用默认数据源  
datasource:  
  master:  
    url: jdbc:mysql://127.0.0.1:3306/niua-easy  #主数据库地址
    username: root  # 数据库账号
    password: 123456 # 数据库密码
    driver-class-name: com.mysql.jdbc.Driver # 3.2.0开始支持SPI可省略此配置  
  slave_1:  
    url: jdbc:mysql://127.0.0.1:3306/niua-test  #从数据库地址
    username: root  #数据库账号
    password: 123456789  #数据库地址
    driver-class-name: com.mysql.jdbc.Driver # 3.2.0开始支持SPI可省略此配置
```

- 修改上传文件路径及文档路径

修改 `application-local.yml` (您所对应的文件)

```yml
# 项目相关配置  
niua:  
  # 名称  
  name: NiuA  
  # 版本  
  version: 0.0.1  
  # 版权年份  
  copyrightYear: 2020  
  # 实例演示开关  
  demoEnabled: true  
  profileMap: /profile/**  
  # 文件路径 示例（ Windows配置D:/niua/uploadPath/，Linux配置 /home/niua/uploadPath/）  
  profile: G:\\Code\\niua\\uploadPath\\  
  # 文档路径  
  docPath: G:\\Code\\niua\\docs\\  
  # 获取ip地址开关  
  addressEnabled: false  
  # 验证码类型 math 数组计算 char 字符验证  
  captchaType: math
```

- 修改代码生成配置文件

在 `niua-gen/src/main/resources/` 目录下，找到 `gen.yml`

修改代码生成路径信息

```yml
# 代码生成  
gen:   
  # 作者  
  author: niua  
  # 默认生成包路径 system 需改成自己的模块名称 如 system monitor tool  
  packageName: tech.niua.admin  
  suffixPath: /fileSuffix  
  # 自动去除表前缀，默认是false  
  autoRemovePre: false  
  # 表前缀（生成类名不会包含表前缀，多个用逗号分隔）  
  tablePrefix: sys_  
  # 数据库设置  
  url: ${spring.datasource.dynamic.datasource.master.url}  
  # 用户名  
  data-username: ${spring.datasource.dynamic.datasource.master.username}  
  # 密码  
  data-password: ${spring.datasource.dynamic.datasource.master.password}  
  # 驱动  
  driverClassName: ${spring.datasource.dynamic.datasource.master.driver-class-name}  
  # 代码生成位置  
  outPath: G:\\Code\\codeGeneration\\
```