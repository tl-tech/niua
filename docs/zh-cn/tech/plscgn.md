# 批量删除

除了删除，往往还会用到批量删除，批量删除与删除调用同一个接口，只是在前端略有不同。

函数主体：

```javascript
batchDelete: function () {
      let that = this;
      let _rows = this.$data.tableChecked;
      if (_rows.length <= 0) {
        this.$message.warning('请先选择需要删除的数据');
        return;
      }
      that.$confirm('是否删除选中的所有用户?删除后无法恢复!', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        _rows.forEach(element => {
          that.ids.push(element.id)
        })
        this.delete();
      }).catch(() => {
        this.$message({
          type: 'info',
          message: '已取消删除'
        });
      });
    },
    //删除ids中对应的
    delete: function () {
      let data = this.$data.ids
      //获得url尾部,要删除的id部分
      let urlChild = '';
      data.forEach((e) => {
        urlChild += 'ids=' + e + '&'
      });
      urlChild = urlChild.substring(0, urlChild.lastIndexOf('&'));

      let url = this.$data.urls.delete + '/?' + urlChild
      this.axios({
        method: 'GET',
        url: url,
        data: {}
      }).then(res => {
        let code = res.data.code;
        this.$data.ids = [];//清空要删除的id数组
        if (code == 200) {
          this.pageList();
        }
      }).catch(error => {
        console.log(error);
      });
    },
```

后端代码：

```java
    /**
    * 删除
    * @param ids
    * @return
    */
    @Log(value = "删除", businessType = BusinessType.DELETE)
    @PreAuthorize("hasAuthority('/test/delete')")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    @GetMapping("/delete")
    public ResultJson delete(@RequestParam("ids") Long[] ids){
        UpdateWrapper<Test> updateWrapper = new UpdateWrapper<>();
        updateWrapper.in("id",ids).set("delete_flag",1);
        boolean update = testService.update(null, updateWrapper);
        if(update) return ResultJson.ok();
        return ResultJson.failure(ResultCode.SERVER_ERROR);
    }

```

总结：点击批量删除后，如果有选中行，就将选中行的id逐一放到ids中，之后调用`delete`函数，流程与单个删除完全相同。
