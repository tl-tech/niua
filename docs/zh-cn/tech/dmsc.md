# 代码生成

## 代码生成规则

---

1. 不生成对应字段的搜索框及展示列表

> 数据库的注释字段中加入感叹号 `!`，中文或英文感叹号均可

使用此方法代表不生成相应的搜索框和显示列表，但是在添加和编辑中依旧生成

---

2. 生成下拉框规则

> - 要求数据库字段名称为： `<字段名称>_status`
> - 
> - 数据库字段注释如下格式： `状态#0:不可用#1:可用`
> 
>> 状态代表生成的下拉框的 `label`
>> 
>> `#` 用来分割每行
>> 
>> `:` 用来分割 `value` 和要展示的内容

---

3. `delete_flag` 删除标识字段

若数据库的表中存在该字段，则会使生成的代码有一个删除标识（即未删除和已删除状态字段）

删除数据后并不会在数据库进行物理删除，而是把 `delete_flag` 删除标识字段置为 `1` (0代表正常，1代表已被删除)，`delete_flag` 为 `1` 的数据不会进行展示

---

4. 一对一代码生成

> 规则： `<字段注释>#join#<关联表表名>#<关联表中要关联的字段>#<要展示关联表中的哪一条信息>#<标签内容>`

- 例如：一个学生对应一个班级

学生表 `t_student`

| 字段 | 类型 | 注释  |
| --- | --- |-----|
|id|bigint| ID  |
|name|varchar| 姓名  |
|clazz_id|bigint|班级#join#t_clazz#id#name#班级名称|

班级表 `t_clazz`

| 字段 | 类型 | 注释 |
| --- | --- | --- |
|id|bigint|ID|
|name|varchar|班级名称|
|teacher| varchar | 教师名称|

学生表中 `clazz_id` 字段注释解释如下：

> 我们将 `班级#join#t_clazz#id#name#班级名称` 这个注释看作一个数组，以 `#` 分割元素
>
>> 注释[0] 为 "班级"，其代表 `clazz_id` 字段最初始的注释，及解释 `clazz_id` 字段的含义
>>
>> 注释[1] 为 "join"，该处要求必须为 `join`
>>
>> 注释[2] 为 "t_clazz"，表示 `clazz_id` 字段将要去 `t_clazz` 表中获取数据
>>
>> 注释[3] 为 "id"，表示 `clazz_id` 是与 `t_clazz` 表中的 `id` 字段关联的
>>
>> 注释[4] 为 "name"，表示 `clazz_id` 字段最终在页面中展示的是 `t_clazz` 表中 `name` 的信息
>>
>> 注释[5] 为 "班级名称"，表示 `clazz_id` 在页面中生成的下拉框的 `label` 的内容

> TIPS
>
> 1. 要以 `#` 分割注释
> 2. 注释[1] 的 `join` 为一对一代码生成必写项，不可修改
> 3. `t_student` 中 `clazz_id` 的类型要与 `t_clazz` 中 `id` 的类型一致

---

5. 一对多代码生成

> 字段规则： 必须要有字段 `relation_mark`
>
> 注释规则： `!#rel#<一对多中 '多' 代表的一方的表名'>`

- 例如：一个班级对应多个学生

学生表 `t_student`

| 字段 | 类型 | 注释  |
| --- | --- |-----|
|id|bigint| ID  |
|name|varchar| 姓名  |
|clazz_id|bigint|班级|

班级表 `t_clazz`

| 字段 | 类型 | 注释 |
| --- | --- | --- |
|id|bigint|ID|
|name|varchar|班级名称|
|teacher| varchar | 教师名称|
|relation_mark|bigint|!#rel#t_student|

> 我们将 `!#rel#t_student` 这个注释看作一个数组，以 `#` 分割元素
>
>> 注释[0] 为 "!"，此为规则，必填，不可修改
>>
>> 注释[1] 为 "rel"，此为规则，必填，不可修改
>>
>> 注释[2] 为 "t_student"，代表当前表将要获取 `t_student` 表中与当前表某一条记录有关联的所有的信息

> TIPS
>
> 1. 要以 `#` 分割注释
> 2. 两张表中均要使用 `id` 作为主键
> 3. 在代码生成的 `Vue` 文件最下方提供了访问 一对多 中两表中所有与一有关的信息的方法 (`viewHandleClick(id)`)
> 4. `viewHandleClick(id)` 的参数要求是 一对多 中 '一'代表的一方的主键(id)
> 5. 上述方法在后端的接口为 `/view关联表名称去前缀/{id}`
> 6. `relation_mark` 字段的类型要与该表主键的类型保持一致