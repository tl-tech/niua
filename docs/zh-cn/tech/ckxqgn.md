## 查看功能

查看功能用于查看某一行的具体信息，不需要新的请求，直接调用后台findById接口即可，相当于一个不修改数据的修改功能

函数主体：

```javascript
detaillHandleClick: function (row) {
      this.$data.loading = true;
      this.isEditor = true;
      let url = this.$data.urls.find + "/" + row.id;
      this.axios({
        method: "GET",
        url: url,
        data: {},
      }).then((res) => {
        this.$data.loading = false;
        let code = res.data.code;
        if (code == 200) {
          this.dialogAddFormVisible = true;
          this.resetDialogFormData('dialogForm');
          this.$nextTick(() => {
            this.$data.dialogForm = res.data.data;
          })
        }
      }).catch((error) => {
        console.log(error)
      });
    },
```

参数说明：

- `row`object对象格式，内有操作行的全部信息

总结：`detailHandleClick`中，相当于不调用增加修改接口的修改功能，只需要将当前行的id作为参数请求后端接口，将返回来的数据以特定的形式呈现即可。

调用：

```vue
//通过slot-scope="scope"获取当前操作行对象，绑定点击事件调用detailHandleClick
<el-table-column fixed="right" label="操作">
        <template slot-scope="scope">
          <el-button size="small"
                     type="text" @click="detailHandleClick(scope.row)">查看详情
          </el-button>
        </template>
      </el-table-column>

```
