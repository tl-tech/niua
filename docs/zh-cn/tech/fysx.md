# 后台手册

## 分页实现

---

- 前端基于饿了么团队的 `element ui` 封装的分页组件 `Pagination` ，并且该框架对分页组件的完整形式进行了二次封装
- 后端基于 `Mybatis-Plus` 的分页组件的 `IPage` 进行实现

### 前端调用实现

1. 前端定义分页流程

```js
// 页面添加分页组件，传入分页变量
<template>
  <div>
    <pagination :total="totalCount" 
	    :current-page="currentPage" 
	    :page-size="pageSize"  
	    @handleCurrentChange="handleCurrentChange" // 页码改变时调用的函数
	    @handleSizeChange="handleSizeChange"> // 页面大小改变时调用的函数
    </pagination>
  <div>
</template>
<script>
import Pagination from '@/components/Pagination/Pagination'
export default {
  components: {
    Pagination,
  },
  data() {
	// 一般在查询参数中定义分页变量
    totalCount: 0,
    currentPage: 1,
    pageSize: 10,
  },
  methods: {
	pageList: function () {  
	    this.$data.loading = true;  
	    let url = this.$data.urls.search + "/" + this.currentPage + "/" + this.pageSize;  
	    this.axios({  
	        method: "POST",  
	        url: url,  
	        data: this.$data.searchForm,  
	    }).then((res) => {  
	        this.$data.loading = false;  
	        let code = res.data.code;  
	        if (code == 200) {  
	            this.$data.tableData = res.data.data.records;  
	            this.$data.totalCount = res.data.data.total;  
	            this.$data.currentPage = res.data.data.current;  
	        }  
	    }).catch((error) => {  
	        console.log(error);  
	    });  
	},
	handleCurrentChange: function (val) {  
	    this.$data.currentPage = val;  
	    this.pageList();  
	},  
	//改变每页显示数量，重新加载数据  
	handleSizeChange: function (val) {  
	    this.$data.currentPage = 1;  
	    this.$data.pageSize = val;  
	    this.pageList();  
	},
  }
}

</script>
```