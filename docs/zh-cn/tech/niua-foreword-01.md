# niua简介及运行安装
> 牛蛙（niua）让编程从未如此简单

# niua项目简介

> niua项目是一套完全由java编写的后台管理系统，主要目的是提供一套完整的后台管理框架，
集成权限验证和代码生成，简化开发人员开发流程和工作量，让开发人员节省出更多时间，做更伟大的事。

# 主要模块说明
#### 1、模块说明
niua_admin 模块为业务模块，所有业务相关代码都写在里面

niua_auth  为权限验证模块，所有权限验证相关逻辑都写在里面

niua_common 为公用模块，可以公用的工具类和配置也在里面

niua_core 核心配置组件，项目核心的配置都写在里面

niua_gen 代码生成相关业务

niua_quartz 任务调度相关

easy-backend-ui 后台页面

docs 文档页面


#### 2、本地化部署需要注意的细节
admin模块下的yml文件

1）需要修改profile参数，此路径存储相关的上传和下载资源路径，需要根据自己的系统进行配置

2）gen.yml

修改outPath参数，此参数为代码生成的目标路径

#### 3、 初始化数据库
在niua-admin模块下的initsql文件夹下

#### 4、运行后台程序

在niua-admin模块下，运行NiuaApplication

#### 5、运行后台界面
* 先安装nodeJS.
* 进入easy-backend-ui目录
* 执行npm install
* 执行完毕后，在执行npm run serve
* 根据输出结果，访问前端页面

![图片](./images/WX20210701-221337.png)



  
  