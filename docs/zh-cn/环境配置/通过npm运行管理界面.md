## 通过npm运行管理端界面

1、 现在安装nodejs

访问网址：https://nodejs.org/en/ 

选择对应的安装目录，直接点击下一步进行安装。



2、 安装python环境

访问网址：https://www.python.org/

配置python环境变量



3、进入easy-backend-ui目录，执行

```bash
npm install
```



4、执行成功后，启动项目

```bash
npm run serve
```



5、问题

######  高版本node导致 Error: Node Sass version 7.0.1 is incompatible with ^4.0.0.异常

1) 卸载node-sass

```bash
npm uninstall node-sass 
```

2) 安装sass

   ```bash
   npm install sass@1.26.5 --save-dev 
   ```

   

3) 安装sass-loader

   ```bash
   npm install sass-loader@7.3.1 
   ```

   

4) 执行npm install 和npm run serve查看是否能正常运行