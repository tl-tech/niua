### ubuntu16.04安装docker和docker compose（使用阿里云镜像源安装）

#### 一、安装 docker

1、 卸载旧版本的docker

> 全新安装时，无需执行步骤

``` bash
sudo apt-get remove docker docker-engine docker.io containerd runc
```

2、更新系统源

```bash
sudo apt-get update
```

3、安装允许apt使用基于https的仓库安装软件

```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

4、 添加GPG密钥

```bash
curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
```

> 然后验证密钥是否添加成功，输入指令

```bash
sudo apt-key fingerprint 0EBFCD88
```

5、写入docker stable版本的阿里云镜像软件源

```bash
sudo add-apt-repository \
   "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
```

6、更新软件源

```bash
sudo apt-get update
```

7、安装最新版的docker ce

```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

8、通过运行hello-world验证docker ce安装成功

```bash
sudo docker run hello-world
```

9、启动docker

```bash
systemctl start docker
```

#### 二、安装docker compose

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

1、授权

```bash
sudo chmod +x /usr/local/bin/docker-compose
```

2、查看docker compose的版本

```bash
docker-compose --version
```

#### 三、docker建立用户组

> 这一步的目的是非root用户使用docker时，因为权限问题，会出现一些看上去很奇怪的问题，以下步骤是解决因权限产生的使用docker的问题的方法。

1、 创建docker用户组

```bash
sudo groupadd docker
```

2、将你的用户加入该组

```bash
sudo usermod -aG docker ${USER}
```

3、重启docker服务

```bash
sudo systemctl restart docker
```

4、将用户切入进该组

```
newgrp - docker
```

