package ${package.ServiceImpl};

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
<#assign flag=0 />
<#list table.fields as field >
 <#if field.customMap.relationFlag>
  <#assign flag=flag+1 />
import ${cfg.pkgName}.${field.customMap.relationCommentModel.beanObjName?uncap_first}.domain.${field.customMap.relationCommentModel.beanObjName};
  <#if flag!=1>
   <#continue />
  </#if>
import ${cfg.voPackage}.${entity}VO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
 </#if>
 <#if field.customMap.specialFieldFlag>
import ${cfg.pkgName}.${entity?uncap_first}.vo.${entity}VO;
 </#if>
</#list>
/**
 * <p>
 *  ${table.comment} 服务实现类
 * </p>
 *
 * @author niua
 * @since ${.now?string("yyyy-MM-dd HH:mm:ss")}
 */
@Service
public class ${table.serviceImplName} extends ServiceImpl<${table.mapperName}, ${table.entityName}> implements ${table.serviceName} {
<#assign flag=0 />
<#list table.fields as field>
<#if field.customMap.relationFlag>
 <#if flag==0>
    public IPage<${entity}VO> pageList(IPage<${entity}VO> page, QueryWrapper<${entity}> ew){
        return this.baseMapper.pageList(page, ew);
    }
 </#if>
  <#assign flag=flag+1 />
    public List<${field.customMap.relationCommentModel.beanObjName}> find${field.customMap.relationCommentModel.aliasVar?cap_first}(){
        return this.baseMapper.find${field.customMap.relationCommentModel.aliasVar?cap_first}();
    }
  </#if>
  <#if field.customMap.specialFieldFlag>
   @Override
    public ${entity}VO view${field.customMap.specialFieldCommentModel.beanObjName}(${field.propertyType} id) {
        return this.baseMapper.view${field.customMap.specialFieldCommentModel.beanObjName}(id);
    }
  </#if>
  </#list>

}
