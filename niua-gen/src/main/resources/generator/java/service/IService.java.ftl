package ${package.Service};

import com.baomidou.mybatisplus.extension.service.IService;
import ${package.Entity}.${entity};
<#assign flag=0 />
<#list table.fields as field >
 <#if field.customMap.relationFlag>
     <#assign flag=flag+1 />
import ${cfg.pkgName}.${field.customMap.relationCommentModel.beanObjName?uncap_first}.domain.${field.customMap.relationCommentModel.beanObjName};
     <#if flag!=1>
         <#continue />
     </#if>
import ${cfg.voPackage}.${entity}VO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
 </#if>
    <#if field.customMap.specialFieldFlag>
import ${cfg.pkgName}.${entity?uncap_first}.vo.${entity}VO;
    </#if>
</#list>

/**
 * <p>
 * ${table.comment} 服务类
 * </p>
 *
 * @author niua
 * @since ${.now?string("yyyy-MM-dd HH:mm:ss")}
 */
public interface ${table.serviceName} extends IService<${table.entityName}> {
<#assign flag=0 />
<#list table.fields as field>
    <#if field.customMap.relationFlag>
    List<${field.customMap.relationCommentModel.beanObjName}> find${field.customMap.relationCommentModel.aliasVar?cap_first}();
        <#if flag!=0>
            <#continue>
        </#if>
    IPage<${entity}VO> pageList(IPage<${entity}VO> page, QueryWrapper<${entity}> ew);
        <#assign flag=flag+1 />
    </#if>
  <#if field.customMap.specialFieldFlag>
    ${entity}VO view${field.customMap.specialFieldCommentModel.beanObjName}(${field.propertyType} id);
  </#if>
</#list>

}