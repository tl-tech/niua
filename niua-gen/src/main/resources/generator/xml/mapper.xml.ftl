<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package.Mapper}.${entity}Mapper">
    <resultMap id="BaseResultMap" type="${package.Entity}.${entity}">
        <#list cfg.mybatisFields as field >
            <id column="${field.column}" jdbcType="${field.jdbcType}" property="${field.property}" />
        </#list>
    </resultMap>
    <sql id="Base_Column_List">
        <#list table.commonFields as field >
            <#if !field_has_next>
                ${field.name},
            </#if>
        </#list>
        ${table.fieldNames}
    </sql>
    <#--  一对一 BEGIN  -->
    <#assign flag=0 />
    <#list table.fields as field>
        <#if flag==0>
            <#if field.customMap.relationFlag>
                <#assign flag=flag+1 />
                <select id="pageList" resultType="${cfg.voPackage}.${entity}VO">
                    SELECT
                    ${table.name}.*,
                    <#assign mark=0 />
                    <#list table.fields as fi>
                        <#if fi.customMap.relationFlag>
                            <#if mark==0>
                                ${fi.customMap.relationCommentModel.tableName}.`${fi.customMap.relationCommentModel.showFieldxml}` as ${fi.customMap.relationCommentModel.showAlias}
                            <#else >
                                , ${fi.customMap.relationCommentModel.tableName}.`${fi.customMap.relationCommentModel.showFieldxml}` as ${fi.customMap.relationCommentModel.showAlias}
                            </#if>
                            <#assign mark=mark+1 />
                        </#if>
                    </#list>
                    FROM
                    ${table.name}

                    <#assign re=0 />
                    <#list table.fields as fe>
                        <#if fe.customMap.relationFlag>
                            <#if re==0>
                                LEFT JOIN
                                ${fe.customMap.relationCommentModel.tableName}
                                ON
                                ${table.name}.${fe.name} = ${fe.customMap.relationCommentModel.tableName}.${fe.customMap.relationCommentModel.tablePrimary}
                            <#else >
                                LEFT JOIN
                                ${fe.customMap.relationCommentModel.tableName}
                                ON
                                ${table.name}.${fe.name} = ${fe.customMap.relationCommentModel.tableName}.${fe.customMap.relationCommentModel.tablePrimary}
                            </#if>
                            <#assign re=re+1 />
                        </#if>
                    </#list>
                    <#noparse>$</#noparse>{ew.customSqlSegment}
                </select>
            </#if>
        </#if>
        <#if field.customMap.relationFlag>
            <select id="find${field.customMap.relationCommentModel.aliasVar?cap_first}" resultType="${cfg.pkgName}.${field.customMap.relationCommentModel.beanObjName?uncap_first}.domain.${field.customMap.relationCommentModel.beanObjName}">
                select * from ${field.customMap.relationCommentModel.tableName} where delete_flag=0;
            </select>
        </#if>

        <#if field.customMap.specialFieldFlag>
            <select id="view${field.customMap.specialFieldCommentModel.beanObjName}" parameterType="${field.propertyType}" resultMap="${field.customMap.specialFieldCommentModel.beanObjName?uncap_first}Map">
                select * from ${table.name} where id=<#noparse>#</#noparse>{id}
            </select>
            <resultMap id="${field.customMap.specialFieldCommentModel.beanObjName?uncap_first}Map" type="${cfg.voPackage}.${entity}VO">
                <#list cfg.mybatisFields as fd >
                    <id column="${fd.column}" jdbcType="${fd.jdbcType}" property="${fd.property}" />
                </#list>
                <collection property="${field.customMap.specialFieldCommentModel.beanObjName?uncap_first}List" javaType="java.util.List" ofType="${cfg.pkgName}.${field.customMap.specialFieldCommentModel.beanObjName?uncap_first}.domain.${field.customMap.specialFieldCommentModel.beanObjName}" column="id" select="query${field.customMap.specialFieldCommentModel.beanObjName}List"/>
            </resultMap>
            <select id="query${field.customMap.specialFieldCommentModel.beanObjName}List" parameterType="${field.propertyType}" resultType="${cfg.pkgName}.${field.customMap.specialFieldCommentModel.beanObjName?uncap_first}.domain.${field.customMap.specialFieldCommentModel.beanObjName}">
                select * from ${field.customMap.specialFieldCommentModel.relationTableName} where
                <#assign layer = table.name?split("_") />
                <#assign number = layer?size />
                <#if number == 1>
                    ${entity?uncap_first}_id=<#noparse>#</#noparse>{param}
                <#else>
                    <#assign flag = 0 />
                    <#list layer as ly><#assign flag=flag+1/><#if flag==1><#continue /></#if>${ly}_</#list>id=<#noparse>#</#noparse>{param}
                </#if>
            </select>
        </#if>
    </#list>
    <#--  一对一 END  -->


    <!--注意:在打开下面注释的时候，在执行添加和更新操作时，一定要添加事物，否则会造成无法添加和更新-->
    <!--
 <select id="selectByPrimaryKey" parameterType="java.lang.Long" resultMap="BaseResultMap">
    select
    <include refid="Base_Column_List" />
    from ${table.name}
    <#list cfg.mybatisFields as field >
        <#if field.keyFlag>
       where ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}}
        </#if>
    </#list>
  </select>
  <delete id="deleteByPrimaryKey" parameterType="java.lang.Long">
    delete from  ${table.name}
     <#list cfg.mybatisFields as field >
        <#if field.keyFlag>
       where ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}}
        </#if>
    </#list>
  </delete>
  <insert id="insert" parameterType="${package.Entity}.${entity}">
    insert into ${table.name} (${table.fieldNames})
    values (
    <#list cfg.mybatisFields as field >
    <#if !field.keyFlag>
    <#if !field_has_next>
         ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}}
    <#else>
         ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}},
    </#if>
    </#if>
    </#list>
    )
  </insert>
  <insert id="insertSelective" parameterType="${package.Entity}.${entity}">
    insert into ${table.name}
    <trim prefix="(" suffix=")" suffixOverrides=",">
     <#list table.fields as field >
      <if test="${field.propertyName} != null">
        ${field.name},
      </if>
    </#list>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides=",">
     <#list  cfg.mybatisFields as field >
     <#if !field.keyFlag>
      <if test="${field.property} != null">
        ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}},
      </if>
     </#if>
    </#list>
    </trim>
  </insert>
  <update id="updateByPrimaryKeySelective" parameterType="${package.Entity}.${entity}">
    update ${table.name}
    <set>
      <#list cfg.mybatisFields as field >
      <#if !field.keyFlag>
       <if test="${field.property} != null">
         ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}},
      </if>
      </#if>
     </#list>
    </set>
     <#list cfg.mybatisFields as field >
        <#if field.keyFlag>
       where ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}}
        </#if>
    </#list>
  </update>
  <update id="updateByPrimaryKey" parameterType="${package.Entity}.${entity}">
    update ${table.name}
    set
     <#list cfg.mybatisFields as field >
      <#if !field.keyFlag>
        ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}},
      </#if>
     </#list>
    <#list cfg.mybatisFields as field >
        <#if field.keyFlag>
       where ${field.column} = <#noparse>#</#noparse>{${field.property},jdbcType=${field.jdbcType}}
        </#if>
    </#list>
  </update>
-->
</mapper>