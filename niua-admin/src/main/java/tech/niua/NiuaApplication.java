package tech.niua;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Wangzhen
 * createAt: 2020/5/29
 */
@SpringBootApplication(exclude= {DruidDataSourceAutoConfigure.class})
public class NiuaApplication {

	public static void main(String[] args) {
		SpringApplication.run(NiuaApplication.class, args);
	}
}
