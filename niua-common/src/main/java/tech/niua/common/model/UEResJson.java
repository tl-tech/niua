package tech.niua.common.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class UEResJson implements Serializable {

    private String state;

    private String url;

    private String title;

    private String original;

}